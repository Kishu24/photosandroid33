package com.example.photosandroid;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.provider.MediaStore;
import android.text.InputType;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.photosandroid.databinding.ActivityViewAlbumContentBinding;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class ViewAlbumContent extends AppCompatActivity {

    private AppBarConfiguration appBarConfiguration;
    //private ActivityViewAlbumContentBinding binding;
    public ArrayList<Album> albums;
    public Album album;
    public ArrayList<Photo> photos;

    private ListView listView;
    private Button addPhotoButton, renameAlbumButton, deleteAlbumButton;

    private CustomPhotoList photoListAdapter;
    private int albumPos;
    private Tag searchTag;

    static final int REQUEST_IMAGE_GET = 1;
    static final int RESULT_DELETE_ALBUM = 2;
    static final int RESULT_DELETE_PHOTO = 3;
    static final int RESULT_NO_MORE_PHOTOS = 4;
    static final int RESULT_BACK = 5;

    static final String PHOTO_DELETE_POSITION = "PhotoDeletePos";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_album_content);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle bundle = getIntent().getExtras();

        searchTag = (Tag) bundle.getSerializable("SearchTag");

        if(searchTag != null){
            photos = Photos.searchResultPhotos;
            addPhotoButton = findViewById(R.id.addPhotoButton);
            renameAlbumButton = findViewById(R.id.renameAlbumButton);
            deleteAlbumButton = findViewById(R.id.deleteAlbumButton);

            addPhotoButton.setVisibility(View.GONE);
            renameAlbumButton.setVisibility(View.GONE);
            deleteAlbumButton.setVisibility(View.GONE);
        }
        else{
            albums = Photos.albums;
            albumPos = bundle.getInt("pos");
            album = albums.get(albumPos);
            photos = album.photos;
        }

        photoListAdapter = new CustomPhotoList(this, photos);
        listView = (ListView) findViewById(R.id.albumThumbnailList);
        listView.setAdapter(photoListAdapter);

        listView.setOnItemClickListener((parent, view, position, id) -> {
            showPhoto(position);
        });

    }

    public void newImageChooser(View view){
        Intent i = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        i.setType("image/*");
        i.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(i, "Choose picture"), REQUEST_IMAGE_GET);
    }

    public void backToHome(View view){
        setResult(RESULT_BACK);
        finish();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){
            if(requestCode == REQUEST_IMAGE_GET){
                Uri chosenImageUri = data.getData();
                File temp = new File(chosenImageUri.getPath());
                Bitmap bitmap = null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), chosenImageUri);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if(chosenImageUri != null){
                    ContextWrapper cw = new ContextWrapper(getApplicationContext());
                    File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
                    File file = new File(directory, temp.getName());
                    if(!file.exists()){
                        FileOutputStream fos = null;
                        try{
                            fos = new FileOutputStream(file);
                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                            fos.flush();
                            fos.close();
                        }
                        catch(Exception e){
                            e.printStackTrace();
                        }
                    }
                    Photos.albums.get(albumPos).addPhoto(file.getAbsolutePath());
                    //Toast.makeText(getApplicationContext(), "Added photo, new album size and uri: " + album.photos.get(0) + " --- " + album.photos.size(), Toast.LENGTH_SHORT).show();
                }
            }
        }
        if(resultCode == RESULT_DELETE_PHOTO){
            Bundle bundle = data.getExtras();
            if(bundle != null){
                album.photos.remove(bundle.getInt(PHOTO_DELETE_POSITION));
                //Toast.makeText(getApplicationContext(), "Deleted photo", Toast.LENGTH_SHORT).show();
            }
            else{
                //Toast.makeText(getApplicationContext(), "Bundle is null", Toast.LENGTH_SHORT).show();
            }
        }
        writeData();
        photoListAdapter.notifyDataSetChanged();
    }

    public void renameAlbum(View view){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Change Name:");
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);

        input.setText((CharSequence) albums.get(albumPos).getName());
        builder.setView(input);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                albums.get(albumPos).setName(input.getText().toString());
                writeData();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }

    public void deleteAlbum(View view){
        Bundle bundle = new Bundle();
        bundle.putInt("DeletePos", albumPos);

        Intent intent = new Intent();
        intent.putExtras(bundle);
        setResult(RESULT_DELETE_ALBUM, intent);
        finish();
    }

    public void showPhoto(int pos){
        Bundle bundle = new Bundle();
        Intent intent = new Intent(this, PhotoDisplay.class);

        bundle.putInt("AlbumPos", albumPos);
        bundle.putInt("PhotoPos", pos);
        bundle.putSerializable("SearchTag", searchTag);
        intent.putExtras(bundle);
        startActivityForResult(intent, 12312);
    }

    public void writeData(){
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(Photos.newFile);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(albums);
            //Toast.makeText(getApplicationContext(), "Wrote to: " + Photos.newFile.getAbsolutePath(), Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            //Toast.makeText(getApplicationContext(), "Exception", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

}