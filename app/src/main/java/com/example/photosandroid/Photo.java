package com.example.photosandroid;

import android.net.Uri;

import java.io.Serializable;
import java.util.ArrayList;

public class Photo implements Serializable {
    /**
     * Arraylist of type tag to store all of the photo tags
     */
    public ArrayList<Tag> tags;
    /**
     * String to hold the filePath
     */
    private String imageUri;
    /**
     * Constructor to build a photo
     * @param imageUri the string that holds the file path of the photo
     */
    public Photo(String imageUri){
        try {
            this.imageUri = imageUri;

            tags = new ArrayList<Tag>();
        }
        catch(Exception e) {
            e.printStackTrace();
        }

    }

    public void addPersonTag(String value) {
        tags.add(new Tag("Person", value));
        Photos.AddPersonTag("Person", value);
    }

    public void addLocationTag(String value) {
        tags.add(new Tag("Location", value));
        Photos.AddLocationTag("Location", value);
    }


    /**
     * toString() to return the file path of the photo as a string
     * @return file path string
     */
    public String toString() {
        return imageUri.toString();
    }
    /**
     * getter for the Uri
     * @return the Uri
     */
    public String getFilePath() {
        return imageUri;
    }
}
