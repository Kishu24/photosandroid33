package com.example.photosandroid;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class Photos extends AppCompatActivity {

    private ListView albumListView;
    private MenuItem searchItem;
    private Menu menu;

    private ArrayAdapter<Album> adapter;
    private ArrayAdapter<Tag> testAdapter;

    public static ArrayList<Album> albums = new ArrayList<Album>();
    public static ArrayList<Tag> personTags = new ArrayList<>();
    public static ArrayList<Tag> locationTags = new ArrayList<>();

    public static ArrayList<Photo> searchResultPhotos = new ArrayList<>();

    static File newFile;

    static final int REQUEST_IMAGE_GET = 1;
    static final int RESULT_DELETE_ALBUM = 2;
    static final int RESULT_DELETE_PHOTO = 3;
    static final int RESULT_NO_MORE_PHOTOS = 4;
    static final int RESULT_BACK = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.album_list);

        newFile = new File(getFilesDir().getAbsolutePath() + "/data.txt");

        FileInputStream fis = null;
        try {
            fis = new FileInputStream(newFile);
            ObjectInputStream oos = new ObjectInputStream(fis);
            albums = (ArrayList<Album>) oos.readObject();

        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }

        LoadTags();

        adapter = new ArrayAdapter<>(this, R.layout.album_view, albums);
        albumListView = findViewById(R.id.album_list);
        albumListView.setAdapter(adapter);
        albumListView.setOnItemClickListener((p, V, pos, id) -> showPhotos(pos));

    }
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.add_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    public void searchByPerson(View view){
        if(personTags.isEmpty())
            Toast.makeText(getApplicationContext(), "There are no person tags!", Toast.LENGTH_SHORT).show();
        else
            SearchDialog(personTags);
    }

    public void searchByLocation(View view){
        if(locationTags.isEmpty())
            Toast.makeText(getApplicationContext(), "There are no location tags!", Toast.LENGTH_SHORT).show();
        else
            SearchDialog(locationTags);
    }

    public void SearchDialog(ArrayList<Tag> tags){
        final AlertDialog.Builder alert = new AlertDialog.Builder(this);
        View view = getLayoutInflater().inflate(R.layout.dialog_tag_search, null);
        testAdapter = new ArrayAdapter<>(this, R.layout.album_view, tags);
        CustomSearchView searchView = (CustomSearchView) view.findViewById(R.id.action_search);
        searchView.setAdapter(testAdapter);

        Button cancel = (Button) view.findViewById(R.id.search_tag_button_cancel);
        alert.setView(view);

        final AlertDialog alertDialog = alert.create();
        searchView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ShowSearchResults((Tag) testAdapter.getItem(position));
                alertDialog.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    public void ShowSearchResults(Tag tag){
        searchResultPhotos.clear();
        for(Album a : albums){
            for(Photo p : a.photos){
                for(Tag t : p.tags){
                    if(tag.equals(t)){
                        searchResultPhotos.add(p);
                        break;
                    }
                }
            }
        }

        if(!searchResultPhotos.isEmpty()){
            Intent intent = new Intent(this, ViewAlbumContent.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("SearchTag", tag);
            intent.putExtras(bundle);
            startActivityForResult(intent, 12312);
        }
        else{
            Toast.makeText(getApplicationContext(), "No photos with that tag!", Toast.LENGTH_SHORT).show();

        }

    }

    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.action_add:
                addAlbum();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void addAlbum(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add new album:");
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                albums.add(new Album(input.getText().toString()));
                adapter.notifyDataSetChanged();
                writeData();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }

    private void showPhotos(int pos){
        Bundle bundle = new Bundle();
        bundle.putInt("pos", pos);
        Intent intent = new Intent(this, ViewAlbumContent.class);
        intent.putExtras(bundle);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivityForResult(intent, RESULT_DELETE_ALBUM);

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_DELETE_ALBUM){
            Bundle bundle = data.getExtras();
            if(bundle != null){
                albums.remove(bundle.getInt("DeletePos"));
            }
        }

        writeData();
        LoadTags();
        adapter.notifyDataSetChanged();
    }

    static void writeData(){
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(newFile);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(albums);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void LoadTags(){
        HashSet<String> tagMap = new HashSet<>();
        personTags.clear();
        locationTags.clear();
        for(Album a : albums){
            for(Photo p : a.photos){
                for(Tag t : p.tags){
                    if(!tagMap.contains(t.toString())){
                        if(t.getName().equals("Person")){
                            personTags.add(t);
                        }
                        else{
                            locationTags.add(t);
                        }
                        tagMap.add(t.toString());
                    }
                }
            }
        }
    }

    public static void AddPersonTag(String name, String value){
        if(personTags != null){
            for(Tag t : personTags){
                if(t.getValue().equalsIgnoreCase(value) && t.getName().equalsIgnoreCase(name))
                    return;
            }
        }
        else{
            personTags = new ArrayList<Tag>();
        }
        personTags.add(new Tag(name, value));
    }

    public static void AddLocationTag(String name, String value){
        if(locationTags != null){
            for(Tag t : locationTags){
                if(t.getValue().equalsIgnoreCase(value) && t.getName().equalsIgnoreCase(name))
                    return;
            }
        }
        else{
            locationTags = new ArrayList<Tag>();
        }
        locationTags.add(new Tag(name, value));
    }

}