package com.example.photosandroid;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CursorAdapter;

import androidx.appcompat.widget.SearchView;

public class CustomSearchView extends SearchView {
    private SearchView.SearchAutoComplete searchAutoComplete;

    public CustomSearchView(Context context){
        super(context);
        searchAutoComplete = (SearchAutoComplete) findViewById(R.id.search_src_text);
        this.setAdapter(null);
        this.setOnItemClickListener(null);
    }

    public CustomSearchView(Context context, AttributeSet attributeSet){
        super(context, attributeSet);
        searchAutoComplete = (SearchAutoComplete) findViewById(R.id.search_src_text);
        this.setAdapter(null);
        this.setOnItemClickListener(null);
    }

    /*
    @Override
    public void setSuggestionsAdapter(CursorAdapter adapter){

    }
    */

    public void setOnItemClickListener(AdapterView.OnItemClickListener listener){
        searchAutoComplete.setOnItemClickListener(listener);
    }

    public void setAdapter(ArrayAdapter<?> adapter){
        searchAutoComplete.setAdapter(adapter);
    }

    public void setText(String text){
        searchAutoComplete.setText(text);
    }

}
