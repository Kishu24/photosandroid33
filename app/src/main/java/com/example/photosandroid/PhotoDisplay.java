package com.example.photosandroid;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.text.InputType;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.navigation.ui.AppBarConfiguration;

import com.example.photosandroid.databinding.ActivityPhotoDisplayBinding;

import java.util.ArrayList;

public class PhotoDisplay extends AppCompatActivity {

    private AppBarConfiguration appBarConfiguration;
    private ActivityPhotoDisplayBinding binding;

    private ArrayList<Album> albums;
    private Album album;
    private ArrayList<Photo> photos;
    private Photo photo;
    private ArrayList<Tag> tags;
    private Tag searchTag;

    private int albumPos, photoPos, tagIndex;

    private ImageView photoImageView;
    private ListView tagListView;
    private Button deletePhotoButton, moveToOtherAlbumButton;

    private String newTagValue;
    private ArrayAdapter<Tag> adapter;

    static final int REQUEST_IMAGE_GET = 1;
    static final int RESULT_DELETE_ALBUM = 2;
    static final int RESULT_DELETE_PHOTO = 3;
    static final int RESULT_NO_MORE_PHOTOS = 4;

    static final String PHOTO_DELETE_POSITION = "PhotoDeletePos";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityPhotoDisplayBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setSupportActionBar(binding.toolbar);

        albums = Photos.albums;
        deletePhotoButton = findViewById(R.id.deletePhotoButton);
        moveToOtherAlbumButton = findViewById(R.id.moveToOtherAlbumButton);

        Bundle bundle = getIntent().getExtras();

        if(bundle != null){
            searchTag = (Tag) bundle.getSerializable("SearchTag");
            photoPos = bundle.getInt("PhotoPos", -1);
            if(searchTag != null){
                photos = Photos.searchResultPhotos;
                Toast.makeText(getApplicationContext(), "why: " + Photos.newFile.getAbsolutePath(), Toast.LENGTH_SHORT).show();
            }
            else{
                albumPos = bundle.getInt("AlbumPos", -1);
                album = albums.get(albumPos);
                photos = album.photos;
            }
            photo = photos.get(photoPos);
            tags = photo.tags;

        }

        photoImageView = (ImageView) findViewById(R.id.photoView);
        photoImageView.setImageURI(Uri.parse(photo.getFilePath()));

        tagListView = (ListView) findViewById(R.id.tagListView);
        adapter = new ArrayAdapter<>(this, R.layout.tag_view, tags);
        tagListView.setAdapter(adapter);
        tagListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                tagIndex = position;
                editDeleteTag();
            }
        });
    }

    public void editDeleteTag(){
        Tag tag = tags.get(tagIndex);
        final AlertDialog.Builder alert = new AlertDialog.Builder(this);
        View view = getLayoutInflater().inflate(R.layout.dialog_edit_delete_tag, null);
        final EditText input = (EditText) view.findViewById(R.id.tag_edit_textinput);
        Button save = (Button) view.findViewById(R.id.tag_button_save);
        Button delete = (Button) view.findViewById(R.id.tag_button_delete);
        Button cancel = (Button) view.findViewById(R.id.tag_button_cancel);
        alert.setView(view);
        final AlertDialog alertDialog = alert.create();

        input.setText(tag.getValue());
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tag.setValue(input.getText().toString());
                adapter.notifyDataSetChanged();
                alertDialog.dismiss();
                Photos.writeData();
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tags.remove(tagIndex);
                adapter.notifyDataSetChanged();
                alertDialog.dismiss();
                Photos.writeData();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    public void addPersonTag(View view){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add new person tag:");
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                newTagValue = input.getText().toString();
                photo.addPersonTag(newTagValue);
                adapter.notifyDataSetChanged();
                Photos.writeData();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    public void addLocationTag(View view){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add new location tag:");
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                newTagValue = input.getText().toString();
                photo.addLocationTag(newTagValue);
                adapter.notifyDataSetChanged();
                Photos.writeData();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    public void deletePhoto(View view){
        Bundle bundle = new Bundle();
        bundle.putInt(PHOTO_DELETE_POSITION, photoPos);

        Intent intent = new Intent();
        intent.putExtras(bundle);
        setResult(RESULT_DELETE_PHOTO, intent);
        finish();
    }

    public void previousPhoto(View view){
        if(photoPos > 0){
            loadPhotoData(photoPos - 1);
        }
        else{
            Toast.makeText(getApplicationContext(), "Reached start of album!", Toast.LENGTH_SHORT).show();
        }

    }

    public void nextPhoto(View view){
        if(photoPos < photos.size() - 1){
            loadPhotoData(photoPos + 1);
        }
        else{
            Toast.makeText(getApplicationContext(), "Reached end of album!", Toast.LENGTH_SHORT).show();
        }
    }

    public void loadPhotoData(int pos){
        photoPos = pos;
        photo = photos.get(pos);
        tags = photo.tags;
        photoImageView.setImageURI(Uri.parse(photo.getFilePath()));
        adapter = new ArrayAdapter<>(this, R.layout.tag_view, tags);
        tagListView.setAdapter(adapter);
    }

    public void movePhoto(View view){
        movePhotoMethod();
    }

    public void movePhotoMethod(){
        final AlertDialog.Builder alert = new AlertDialog.Builder(this);
        View view = getLayoutInflater().inflate(R.layout.dialog_move_photo, null);
        Button save = (Button) view.findViewById(R.id.move_photo_button_save);
        Button cancel = (Button) view.findViewById(R.id.move_photo_button_cancel);

        Spinner albumsDropdown = (Spinner) view.findViewById(R.id.move_photo_dropdown);
        ArrayAdapter<Album> albumDropdownAdapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, albums);
        albumsDropdown.setAdapter(albumDropdownAdapter);

        alert.setView(view);
        final AlertDialog alertDialog = alert.create();

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Album movingToAlbum = (Album) albumsDropdown.getSelectedItem();
                if(movingToAlbum != album){
                    Photo movingPhoto = photos.remove(photoPos);
                    movingToAlbum.photos.add(movingPhoto);
                    Photos.writeData();
                    TryLoadNewPhoto();
                }
                else{
                    Toast.makeText(getApplicationContext(), "Can't move a photo to the same album!", Toast.LENGTH_SHORT).show();
                }
                alertDialog.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    public void TryLoadNewPhoto(){
        if(photos.isEmpty()){
            setResult(RESULT_NO_MORE_PHOTOS);
            finish();
        }
        else{
            loadPhotoData(0);
        }
    }

}