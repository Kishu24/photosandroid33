package com.example.photosandroid;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

public class CustomPhotoList extends ArrayAdapter {

    private ArrayList<Photo> photosList;
    private Context context;

    public CustomPhotoList(Context context, ArrayList<Photo> photosList){
        super(context, R.layout.photo_thumbnail_item);
        this.context = context;
        this.photosList = photosList;
    }

    @Override
    public int getCount(){
        return photosList.size();
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.photo_thumbnail_item, parent, false);
        }
        TextView textViewUri = (TextView) convertView.findViewById(R.id.textViewUri);
        ImageView imageThumbnail = (ImageView) convertView.findViewById(R.id.imageViewThumbnail);

        textViewUri.setText(photosList.get(position).getFilePath().toString());

        Uri tempUri = Uri.parse(photosList.get(position).getFilePath());
        imageThumbnail.setImageURI(tempUri);
        return convertView;
    }
}
