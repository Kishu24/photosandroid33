package com.example.photosandroid;

import android.net.Uri;

import java.io.Serializable;
import java.util.ArrayList;

public class Album implements Serializable {
    private String name;

    public ArrayList<Photo> photos;

    public Album(String name){
        this.name = name;
        this.photos = new ArrayList<Photo>();
    }

    public void addPhoto(String uri){
        photos.add(new Photo(uri));
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString(){
        return name + " | " + photos.size() + " photos";
    }

}