package com.example.photosandroid;

import java.io.IOException;
import java.io.Serializable;
import java.util.Locale;

public class Tag implements Serializable {
    private String name;
    private String value;

    public Tag(String name, String value) {
        this.name = name;
        this.value = value;
    }

    @Override
    public String toString(){
        return value + " - " + name;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value){
        this.value = value;
    }

    @Override
    public boolean equals(Object o){
        if(o == this)
            return true;
        if(!(o instanceof Tag))
            return false;
        Tag t = (Tag) o;

        return ((Tag) o).getName().equalsIgnoreCase(this.getName()) && ((Tag) o).getValue().equalsIgnoreCase(this.getValue());
    }

}